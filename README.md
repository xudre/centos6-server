# CentOS 6 (x64) - Setup Script #

Bash script for CentOS 6 setup as web server.

##Includes:##

* Yum base update;
* Apache (httpd) * **
* PHP * **
* Node.js **
* MySQL * **
* MongoDB **
* ISC BIND * **

\* Yum version.
\** Optional.

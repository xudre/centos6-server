#!/bin/bash

# Arquivo de configuração padrão para
# um servidor padrão LAMP em CentOS 6 x86_64

echo "==========================================================="
echo "| Instalando e configurando servidor para CentOS 6 x86_64 |"
echo "==========================================================="
echo
echo "Essa instalação contempla as seguintes etapas:"
echo
echo "  I. Atualiza aplicativos base"
echo " II. Apache"
echo "III. *PHP"
echo " IV. *Node.js"
echo "  V. *MySQL"
echo " VI. *MongoDB"
echo "VII. *ISC BIND"
echo
echo "* opcional"


# Atualizando os aplicativos do sistema
echo ;
echo "Atualizando yum e sistema..."

yum -q -y update yum ;
yum -q -y install yum-utils ;
yum-complete-transaction -q -y ;
yum -q -y update ;
yum -q -y install vim ;

echo "Sistema atualizado."

cp ~/.bash_profile ~/.bash_profile-bkp_setup

echo "alias ll=\"ls -la\""            >> ~/.bash_profile
echo "export LANG=\"pt_BR.utf8\""     >> ~/.bash_profile
echo "export LC_ALL=\"pt_BR.utf8\""   >> ~/.bash_profile

source ~/.bash_profile

echo ;
echo "Criando sistema de arquivos:"

set CFG_CORRECT = "n" ;

while [ "$CFG_CORRECT" != "s" ]; do

  echo ;
  echo -n -e "Defina o `tput bold`diretório raiz`tput sgr0` para o sistema de arquivos: "
  read CFG_SYS_ROOT ;
  echo ;
  echo -n -e "Esse diretório está correto? (s/N) `tput bold`$CFG_SYS_ROOT`tput sgr0` "
  read CFG_CORRECT ;

done;

echo ;
echo -e "1. Diretório de log: $CFG_SYS_ROOT/log"

mkdir -p $CFG_SYS_ROOT/log ;

echo -e "2. Diretório de git: $CFG_SYS_ROOT/git"

mkdir -p $CFG_SYS_ROOT/git ;

echo -e "3. Diretório de downloads de aplicações para o sistema: $CFG_SYS_ROOT/downloads"

mkdir -p $CFG_SYS_ROOT/downloads ;

echo -e "4. Diretório de agentes de monitoramento: $CFG_SYS_ROOT/agent"

mkdir -p $CFG_SYS_ROOT/agent ;

echo -e "5. Diretório de clientes e serviços web: $CFG_SYS_ROOT/web"

mkdir -p $CFG_SYS_ROOT/web ;
cd $CFG_SYS_ROOT/downloads ;

echo ;
echo "Instalando `tput bold`gcc`tput sgr0` ..."

yum -q -y install gcc ;

echo "Instalando `tput bold`git`tput sgr0` ..."

yum -q -y install git ;

echo "Instalando `tput bold`wget`tput sgr0` ..."

yum -q -y install wget ;

# Instalando Apache
echo ;
echo "Instalando `tput bold`Apache (httpd)`tput sgr0` ..."

yum -q -y install httpd ;
cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.backup ;

echo "Iniciando `tput bold`Apache (httpd)`tput sgr0` ..."

/etc/init.d/httpd start ;
/sbin/chkconfig --levels 235 httpd on ;

echo "Instalação do `tput bold`Apache (httpd)`tput sgr0` finalizada."

# Instalando PHP
echo;
echo "Instalando `tput bold`PHP`tput sgr0` ..."

set CFG_CORRECT = "n" ;
echo -n "Deseja prosseguir? (s/N) "
read CFG_CORRECT ;

if [ "$CFG_CORRECT" == "s" ] ; then

  yum -q -y install php php-pear php-mysql php-pdo php-gd php-devel ;
  echo "`tput bold`PHP`tput sgr0` instalado."
  set CFG_PHP_IN = "s" ;

fi ;

# Instalando Node.js
echo ;
echo "Instalando `tput bold`Node.js`tput sgr0` ..."

set CFG_CORRECT = "n" ;
echo -n "Deseja prosseguir? (s/N) "
read CFG_CORRECT ;

if [ "$CFG_CORRECT" == "s" ] ; then

  sudo rpm --import https://fedoraproject.org/static/0608B895.txt ;
  sudo rpm -Uvh http://download-i2.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm ;
  sudo yum install nodejs npm --enablerepo=epel ;
  echo "Instalação do `tput bold`Node.js`tput sgr0` finalizada."
  set CFG_NODEJS_IN = "s" ;

fi ;

# Instalando MySQL
echo ;
echo "Instalando `tput bold`MySQL`tput sgr0` ..."

set CFG_CORRECT = "n" ;
echo -n "Deseja prosseguir? (s/N) "
read CFG_CORRECT ;

if [ "$CFG_CORRECT" == "s" ] ; then

  yum -q -y install mysql-server ;
  yum -q -y install mysql ;

  echo "Iniciando `tput bold`MySQL`tput sgr0` ..."
  /etc/init.d/mysqld start ;
  /sbin/chkconfig --levels 235 mysqld on ;

  echo "A partir de agora você irá configurar sua execução do `tput bold`MySQL`tput sgr0` ..."
  echo ;

  mysql_secure_installation ;
  # GRANT ALL ON * TO 'root'@'%' IDENTIFIED BY '%PASS%';

  echo "Instalação do `tput bold`MySQL`tput sgr0` finalizada."
  set CFG_MYSQL_IN = "s" ;

fi ;

# Instalando MongoDB
echo ;
echo "Instalando `tput bold`MongoDB`tput sgr0` ..."

set CFG_CORRECT = "n" ;
echo -n "Deseja prosseguir? (s/N) "
read CFG_CORRECT ;

if [ "$CFG_CORRECT" == "s" ] ; then

  echo -e "[10gen]\nname=10gen Repository\nbaseurl=http://downloads-distro.mongodb.org/repo/redhat/os/x86_64\ngpgcheck=0" | cat > /etc/yum.repos.d/10gen.repo ;
  yum -q -y install mongo-10gen mongo-10gen-server ;

  echo -e "Inicializando `tput bold`MongoDB`tput sgr0` ..."
  /etc/init.d/mongod start ;
  /sbin/chkconfig --levels 235 mongod on ;

  echo -e "Instalando drivers do `tput bold`MongoDB`tput sgr0` para linguagens do sistema ..."

  # echo ">> CPP" ;

  # echo ">> C" ;

  if [ "$CFG_PHP_IN" == "s" ] ; then

    echo ;
    echo ">> PHP" ;
    mkdir /root/tmpz ;
    mount --bind /root/tmpz /tmp ;
    umount /tmp; umount /var/tmp ;
    pecl install mongo ;
    echo -e "; Enable mongodb extension module\nextension=mongo.so" | cat > /etc/php.d/mongo.ini ;
    echo ">> PHP driver instalado." ;

  fi ;

  if [ "$CFG_NODEJS_IN" == "s" ] ; then

    echo ;
    echo ">> NODEJS" ;
    cp -s /usr/bin/nodejs /usr/bin/node ;
    npm install mongodb ; # To install with C++ bson parser `npm install mongodb --mongodb:native ;`
    echo ">> NODEJS driver instalado." ;

  fi ;

  # echo ">> JAVA" ;

  echo ;
  echo ">> PYTHON" ;
  git clone git://github.com/mongodb/mongo-python-driver.git ~/git/pymongo ;
  cd ~/git/pymongo ;
  python setup.py install ;
  echo ">> PYTHON driver instalado." ;

  echo "Instalação do `tput bold`MongoDB`tput sgr0` finalizada." ;
  set CFG_MONGODB_IN = "s" ;

fi ;

# Instalando ISC BIND
echo ;
echo "Instalando `tput bold`BIND`tput sgr0` ..."

set CFG_CORRECT = "n" ;
echo -n "Deseja prosseguir? (s/N) "
read CFG_CORRECT ;

if [ "$CFG_CORRECT" == "s" ] ; then

  yum -q -y install bind bind-utils bind-chroot ;

fi ;

# Atualiza todas as dependências que foram mitigadas
# durante a instalação dos aplicativos
yum -q -y update ;

# Reinicia o Apache para aplicar alterações.
/etc/init.d/httpd restart ;

echo ;
echo "`tput bold`--------------------------`tput sgr0`" ;
echo "`tput bold`| Instalação finalizada. |`tput sgr0`" ;
echo "`tput bold`--------------------------`tput sgr0`" ;

cd $CFG_SYS_ROOT ;
